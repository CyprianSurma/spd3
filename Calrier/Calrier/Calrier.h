#pragma once

#include <string>
#include <fstream>
#include <deque>
#include <vector>
#include "rpqStruct.h"
#include "Schrage.h"

class Calrier
{
public:

	Calrier(std::string fName);
	~Calrier();
	std::vector<rpqStruct*> tasks;//heap po r
	void calc() { _calc(UINT32_MAX, tasks); }
private:
	void _calc(uint32_t UB, std::vector<rpqStruct*> permutation);
	std::vector<rpqStruct*> bestPermutation;
	uint32_t findMinR(std::vector<rpqStruct*> permutation, uint32_t c, uint32_t b);
	uint32_t findMinQ(std::vector<rpqStruct*> permutation, uint32_t c, uint32_t b);
	uint32_t findPprim(std::vector<rpqStruct*> permutation, uint32_t c, uint32_t b);
	bool FindB(std::vector<rpqStruct*> permutation, uint32_t& b, uint32_t U);
	bool FindA(std::vector<rpqStruct*> permutation, uint32_t& a, uint32_t b, size_t U);
	bool FindC(std::vector<rpqStruct*> permutation, uint32_t a, uint32_t b, uint32_t& c);
};

