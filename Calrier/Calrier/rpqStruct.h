#pragma once

struct rpqStruct
{
	int index;
	int r;
	int p;
	int q;
	rpqStruct(int r, int p, int q, int index) :r(r), p(p), q(q), index(index) {}
	rpqStruct() {};
	~rpqStruct() {};
};

