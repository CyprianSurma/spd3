#pragma once
#include "rpqStruct.h"
#include <string>
#include <deque>
#include <vector>
#include "heap.h"

enum SchrageType
{
	NORMAL_SCHRAGE,
	INT_SCHRAGE,
	NONE_SCHRAGE
};

bool cmp(rpqStruct*&a, rpqStruct*& b) { return a->r < b->r; }
class Schrage
{
public:
	Schrage(Heap<rpqStruct*, decltype(cmp)>& heap, SchrageType type) :type(type) { this->heap = &heap; }

	int getCMax();
	std::deque<rpqStruct*> getPermutation() { return permutation; }
	void sort();
private:
	SchrageType type;
	Heap<rpqStruct*, decltype(cmp)>* heap;
	std::deque<rpqStruct*> permutation;
	std::deque<rpqStruct*> getWaitingTasks(std::deque<rpqStruct*>& allTasks, const int time);
};

