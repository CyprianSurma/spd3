#pragma once
#pragma once
#include <cstdint>

template< class RandomIt, class Compare >
void QuickSort(RandomIt first, RandomIt last, Compare comp)
{

	uint32_t length = last - first;
	if (length <= 1)
	{
		return;
	}
	uint32_t pivot = (length / 2) - 1;
	std::swap(*(first + pivot), (*(last - 1)));

	RandomIt j = first;
	for (RandomIt i = first; i != last; ++i)
	{
		if (comp((*i), (*(last - 1))))
		{
			std::swap((*i), (*j));
			++j;
		}
	}
	std::swap(*(j), (*(last - 1)));

	QuickSort(j + 1, last, comp);
	QuickSort(first, j, comp);
}


template< class RandomIt>
void QuickSort(RandomIt first, RandomIt last)
{
	QuickSort(first, last, [](const decltype(*first) & a, const decltype(*first) & b) -> bool {return a > b; });
}