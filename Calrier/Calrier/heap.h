#pragma once
#include <vector>
#include <iostream>
#include <deque>

template<typename T> auto default_lambda() { return [](T&a, const T&b) {return a < b; }; }
template <typename T, class	Compare = decltype(default_lambda<T>()) >

class Heap
{
public:


	Heap(Compare comp = default_lambda<T>()) :cmp(comp) {};
	~Heap() {};
	bool push(T);
	T top() { return _data[0]; }
	void pop();
	bool empty() { return _data.empty(); }

	void operator+=(Heap<T, Compare> val)
	{
		while (!val.empty())
		{
			push(val.top());
			val.pop();

		}
	}
	void operator+=(std::deque<T> val)
	{
		while (!val.empty())
		{
			push(val.back());
			val.pop_back();

		}
	}
private:
	Compare cmp;
	std::vector<T> _data;
	uint32_t _completedLevels = 0;
	uint32_t _currentLevelMaxSize = 1;
	uint32_t _currentLevelNumOfElements = 0;
};


template <typename T, class Compare = decltype(default_lambda<T>()) >
bool Heap<T, Compare>::push(T elem)
{
	uint32_t elementIndex = _data.size();
	uint32_t parentIndex;
	if (_currentLevelNumOfElements == _currentLevelMaxSize)
	{
		++_completedLevels;
		_currentLevelMaxSize = (uint32_t)pow(2, _completedLevels);
		_currentLevelNumOfElements = 0;
		try {
			_data.reserve(_data.size() + _currentLevelMaxSize);
		}
		catch (std::bad_alloc const&) {
			return false;
		}


	}

	_data.push_back(elem);
	++_currentLevelNumOfElements;
	while (elementIndex != 0)
	{
		parentIndex = (elementIndex - 1) / 2;
		if (cmp(_data[parentIndex], _data[elementIndex]))
		{
			std::swap(_data[parentIndex], _data[elementIndex]);
			elementIndex = parentIndex;
		}
		else
		{
			break;
		}
	}
	return true;
}
template <typename T, class Compare = decltype(default_lambda<T>()) >
void Heap<T, Compare>::pop()
{
	bool isEnd = false;
	uint32_t parentInd = 0;
	uint32_t biggerChildInd, leftInd, rightInd;
	std::swap(_data[0], _data[_data.size() - 1]);
	_data.erase(_data.end() - 1);

	while (!isEnd)
	{
		leftInd = parentInd * 2 + 1;
		rightInd = parentInd * 2 + 2;
		if (leftInd >= _data.size())
		{
			break;
		}
		else
			if (rightInd >= _data.size())
			{
				isEnd = true;
				biggerChildInd = leftInd;
			}
			else
				if (!cmp(_data[leftInd], _data[rightInd]))
				{
					biggerChildInd = leftInd;
				}
				else
				{
					biggerChildInd = rightInd;
				}
		if (!cmp(_data[biggerChildInd], _data[parentInd]))
		{
			std::swap(_data[biggerChildInd], _data[parentInd]);
			parentInd = biggerChildInd;
		}
		else
			break;
	}
}
