#include "Schrage.h"
#include "rpqStruct.h"
#include <iostream>
#include <fstream>
#include <algorithm>

int Schrage::getCMax()
{
	int Cmax = 0;
	int t = 0;
	for (auto& i : permutation)
	{
		int t = std::max(t, i->r) + i->p;
		Cmax = std::max(Cmax, t + i->q);
	}
	return Cmax;
}

void Schrage::sort()
{
	int time = 0;
	std::deque<rpqStruct*> newShedule;

	if (type == NORMAL_SCHRAGE)
	{
		while (!heap.empty())
		{
			std::deque<rpqStruct*> waitingTasks += addWaitingTasks(heap, time);//heap z sortem po q -< dodaj deque
			rpqStruct* highestPrioTask = nullptr;
			if (waitingTasks.empty())
			{
				highestPrioTask = heap.pop();
				time += highestPrioTask->r;
			}
			else
			{
				highestPrioTask = waitingTasks.pop();
			}
			time += highestPrioTask->p;

			newShedule.push_back(highestPrioTask);
		}
	}
	else
	{

		while (!heap->empty())
		{
			std::deque<rpqStruct*> waitingTasks += getWaitingTasks(heap, time);//heap z sortem po q
			rpqStruct* highestPrioTask = nullptr;
			if (waitingTasks.empty())
			{
				highestPrioTask = heap->top();
			}
			else
			{
				highestPrioTask = *waitingTasks.begin();
			}
			--(highestPrioTask->q);
			if (highestPrioTask->index == newShedule.back()->index)
				++(newShedule.back()->p);
			else
			{
				rpqStruct* newStruct = new rpqStruct(highestPrioTask->r, 1, highestPrioTask->q, highestPrioTask->index);
				newShedule.push_back(highestPrioTask);
			}
			++time;
			if(highestPrioTask->p == 0)
				waitingTasks.pop();
		}
	}
	permutation = newShedule;
}

std::deque<rpqStruct*> Schrage::getWaitingTasks(std::deque<rpqStruct*>& allTasks, const int time)
{
	std::deque<rpqStruct*> tasks;

	while ((*allTasks.begin())->r <= time)
	{
		tasks.push_back(allTasks.pop());
	}

	return std::move(tasks);
}