#pragma once
template< class RandomIt, class Compare >
void BubleSort(RandomIt first, RandomIt last, Compare comp)
{

	uint16_t k = 1;

	bool isSwap = true;

	while (isSwap)
	{
		isSwap = false;
		for (auto j = first; j < last - k; ++j)
		{
			if (comp(*(j), *(j + 1)))
			{
				std::swap(*(j), *(j + 1));
				isSwap = true;
			}
		}
		++k;
	}
}



template< class RandomIt>
void BubleSort(RandomIt first, RandomIt last)
{
	BubleSort(first, last, [](const decltype(*first) & a, const decltype(*first) & b) -> bool {return a > b; });
}
