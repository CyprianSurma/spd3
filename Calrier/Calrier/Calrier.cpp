#include "Calrier.h"
#include <algorithm>


Calrier::Calrier(std::string fName)
{
	int garbage;
	int r, p, q;
	std::fstream f(fName);
	if (f.is_open())
	{
		f >> garbage;
		tasks.reserve(garbage);
		f >> garbage;
		while (!f.eof())
		{
			f >> r >> p >> q;
			if (f.eof())
				break;
			tasks.push_back(new rpqStruct(r, p, q, tasks.size() + 1));
		}
	}
	else exit(99);
}


Calrier::~Calrier()
{
	for (auto& i : tasks)
	{
		delete i;
	}
}

void Calrier::_calc(uint32_t UB, std::vector<rpqStruct*> permutation)
{
	Schrage schrage(tasks, NORMAL_SCHRAGE);
	schrage.sort();
	uint32_t U = schrage.getCMax();

	if (U < UB)
	{
		UB = U;
		auto tempPerm = schrage.getPermutation();

		for (uint32_t i = 0; i < permutation.size(); ++i)
			permutation[i] = tempPerm[i];
	}

	uint32_t a, b, c;

	if (FindB(permutation, b, U) == false)
	{
		bestPermutation = permutation;
		return;
	}
	if (FindA(permutation, a, b, U) == false)
	{
		bestPermutation = permutation;
		return;
	}
	if (FindC(permutation, a, b, c) == false)
	{
		bestPermutation = permutation;
		return;
	}

	size_t rPrim = findMinR(permutation, c, b);
	size_t qPrim = findMinQ(permutation, c, b);
	size_t pPrim = findPprim(permutation, c, b);

	uint32_t prevRc = permutation[c]->r;
	permutation[c]->r = std::max(prevRc, rPrim + pPrim);

	Schrage pschrage(tasks, INT_SCHRAGE);
	pschrage.sort();
	uint32_t LB = pschrage.getCMax();

	if (LB < UB)
	{
		_calc(UB, permutation);
	}

	permutation[c]->r = prevRc;

	size_t prevQc = permutation[c]->q;
	permutation[c]->q = std::max(prevQc, qPrim + pPrim);

	Schrage pschrage(tasks, INT_SCHRAGE);
	pschrage.sort();
	uint32_t LB = pschrage.getCMax();

	if (LB < UB)
	{
		_calc(UB, permutation);
	}

	permutation[c]->q = prevQc;
}

bool Calrier::FindB(std::vector<rpqStruct*> permutation, uint32_t& b, uint32_t U)
{
	bool found = false;
	int t = 0;
	int cMax = 0;
	t = std::max(t, permutation[0]->r) + permutation[0]->p;
	cMax = std::max(cMax, t + permutation[0]->q);

	for (int i = 1; i < permutation.size(); i++)
	{
		int temp = t + permutation[i]->p + permutation[i]->q;
		if (temp == U)
		{
			b = i;
			found = true;
		}
		t = std::max(t, permutation[i]->r) + permutation[i]->p;
		cMax = std::max(cMax, t + permutation[i]->q);
	}
	return found;
}

bool Calrier::FindA(std::vector<rpqStruct*> permutation, uint32_t& a, uint32_t b, size_t U)
{
	for (uint32_t i = 0; i < b; ++i)
	{
		uint32_t P = 0;
		for (size_t j = i; j <= b; ++j)
			P += permutation[j]->p;

		size_t temp = permutation[i]->r + P + permutation[b]->q;
		if (U == temp)
		{
			a = i;
			return true;
		}
	}
	return false;
}

bool Calrier::FindC(std::vector<rpqStruct*> permutation, uint32_t a, uint32_t b, uint32_t& c)
{
	for (int j = b; j >= a; --j)
		if (permutation[j]->q < permutation[b]->q)
		{
			c = j;
			return true;
		}
	return false;
}

uint32_t Calrier::findMinR(std::vector<rpqStruct*> permutation, uint32_t c, uint32_t b)
{
	size_t minR = permutation[0]->r;
	for (size_t i = c + 1; i <= b; ++i)
	{
		if (permutation[i]->r < minR)
			minR = permutation[i]->r;
	}
	return minR;
}

uint32_t Calrier::findMinQ(std::vector<rpqStruct*> permutation, uint32_t c, uint32_t b)
{
	size_t minQ = permutation[0]->q;
	for (size_t i = c + 1; i <= b; i++)
	{
		if (permutation[i]->q < minQ)
			minQ = permutation[i]->q;
	}
	return minQ;
}

uint32_t Calrier::findPprim(std::vector<rpqStruct*> permutation, uint32_t c, uint32_t b)
{
	size_t pPrim = 0;
	for (size_t i = c + 1; i <= b; i++)
	{
		pPrim += permutation[i]->p;
	}
	return pPrim;
}